# Use an official Node.js runtime as the base image
FROM node:18

COPY . .

# Install the Angular CLI globally
RUN npm install -g @angular/cli@17.3.7

# Install app dependencies
RUN npm install

# Expose the default Angular development port
EXPOSE 4200

# Start the Angular development server when the container starts
CMD ng serve --host 0.0.0.0

# Set the working directory in the container
WORKDIR /var/www/html
