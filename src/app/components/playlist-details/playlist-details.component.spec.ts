import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PlaylistDetailsComponent } from './playlist-details.component';
import { IPlaylist } from '../../shared/models/playlist.model';
import { ITracks } from '../../shared/models/tracks.model';
import { DeezerService } from '../../services/deezer.service';

describe('PlaylistDetailsComponent', () => {
  let component: PlaylistDetailsComponent;
  let fixture: ComponentFixture<PlaylistDetailsComponent>;
  let deezerServiceSpy: jasmine.SpyObj<DeezerService>;

  const mockPlaylist: IPlaylist = require('../../shared/mocks/data/playlists.mock.json');

  const mockTracks: ITracks[] = require('../../shared/mocks/data/tracks.mock.json');

  beforeEach(async () => {
    deezerServiceSpy = jasmine.createSpyObj('DeezerService', ['getPlaylistDetails', 'getPlaylistTracks']);
    deezerServiceSpy.getPlaylistDetails.and.returnValue(of(mockPlaylist));
    deezerServiceSpy.getPlaylistTracks.and.returnValue(of({ data: mockTracks }));

    await TestBed.configureTestingModule({
      declarations: [PlaylistDetailsComponent],
      providers: [
        { provide: DeezerService, useValue: deezerServiceSpy },
        { provide: ActivatedRoute, useValue: { snapshot: { paramMap: { get: () => '1' } } } }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load playlist details', () => {
    expect(deezerServiceSpy.getPlaylistDetails).toHaveBeenCalledWith(1);
    expect(component.playlist).toEqual(mockPlaylist);
    expect(component.isLoading).toBe(false);
  });

  it('should load playlist tracks', () => {
    expect(deezerServiceSpy.getPlaylistTracks).toHaveBeenCalledWith(1);
    expect(component.tracks).toEqual(mockTracks);
    expect(component.isLoading).toBe(false);
  });
});
