import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ITracks } from '../../shared/models/tracks.model';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DeezerService } from '../../services/deezer.service';
import { IPlaylist } from '../../shared/models/playlist.model';
import { SharedModule } from '../../shared/modules/shared/shared.module';

@Component({
  selector: 'deezer-playlist-details',
  standalone: true,
  imports: [SharedModule, MatListModule, MatIconModule, ScrollingModule, MatTooltipModule],
  providers: [DeezerService],
  templateUrl: './playlist-details.component.html',
  styleUrl: './playlist-details.component.scss'
})
export class PlaylistDetailsComponent implements OnInit {
  public isLoading = true;
  public playlist!: IPlaylist;
  public tracks: ITracks[] = [];
  private playlistId!: number;
  public paginatedTracks: ITracks[] = [];

  constructor(
    private route: ActivatedRoute,
    public deezerService: DeezerService
  ) { }

  public ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.playlistId = +id;
      this.playlistDetails();
      this.playlistTracks();
    }
  }

  private playlistDetails() {
    this.deezerService.getPlaylistDetails(this.playlistId).subscribe(data => {
      this.playlist = data;
      this.deezerService.isLoading.set(false);
    });
  }

  private playlistTracks() {
    this.deezerService.getPlaylistTracks(this.playlistId).subscribe((data) => {
      this.tracks = data.data;
      this.deezerService.isLoading.set(false);
    });
  }
}

