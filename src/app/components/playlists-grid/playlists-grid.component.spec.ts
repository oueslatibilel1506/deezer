import { of } from 'rxjs';
import { IUsers } from '../../shared/models/users.model';
import { DeezerService } from '../../services/deezer.service';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PlaylistsGridComponent } from './playlists-grid.component';


describe('PlaylistsGridComponent', () => {
  let component: PlaylistsGridComponent;
  let fixture: ComponentFixture<PlaylistsGridComponent>;
  let deezerServiceSpy: jasmine.SpyObj<DeezerService>;

  const mockUsersData: IUsers[] = require('../../shared/mocks/data/deezers.mock.json');

  beforeEach(async () => {
    deezerServiceSpy = jasmine.createSpyObj('DeezerService', ['getUserPlaylists']);
    deezerServiceSpy.getUserPlaylists.and.returnValue(of({ data: mockUsersData }));

    await TestBed.configureTestingModule({
      declarations: [PlaylistsGridComponent],
      imports: [MatPaginatorModule],
      providers: [{ provide: DeezerService, useValue: deezerServiceSpy }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load user playlists', () => {
    expect(deezerServiceSpy.getUserPlaylists).toHaveBeenCalledWith(5);
    expect(component.playlists).toBeDefined();
  });

});
