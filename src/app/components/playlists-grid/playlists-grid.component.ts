import { Observable } from 'rxjs';
import { IUsers } from '../../shared/models/users.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DeezerService } from '../../services/deezer.service';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { SharedModule } from '../../shared/modules/shared/shared.module';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'deezer-playlists-grid',
  standalone: true,
  imports: [SharedModule, MatGridListModule, MatPaginatorModule],
  providers: [DeezerService],
  templateUrl: './playlists-grid.component.html',
  styleUrl: './playlists-grid.component.scss'
})
export class PlaylistsGridComponent implements OnInit, OnDestroy {
  private paginator!: MatPaginator;
  public playlists!: Observable<IUsers[]>;
  public breakpoint = 3;

  @ViewChild(MatPaginator, { static: false }) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setPagination();
  }

  public dataSource: MatTableDataSource<IUsers> = new MatTableDataSource<IUsers>([]);

  constructor(public deezerService: DeezerService, private changeDetectorRef: ChangeDetectorRef) { }

  public ngOnInit(): void {
    this.breakpoint = (window.innerWidth <= 400) ? 1 : 3;
    this.deezerService.getUserPlaylists(5).subscribe((data) => {
      this.initializeData(data.data);
    });
  }

  public onResize(event: any) {
    this.breakpoint = (event.target.innerWidth <= 400) ? 1 : 3;
  }

  public ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }
  /* private */

  private initializeData(data: IUsers[]) {
    this.changeDetectorRef.detectChanges();
    this.dataSource = new MatTableDataSource<IUsers>(data);
    this.setPagination();
    this.playlists = this.dataSource.connect();
    this.deezerService.isLoading.set(false);
  }

  private setPagination() {
    this.dataSource.paginator = this.paginator;

  }
}
