import { Routes } from '@angular/router';
import { PlaylistsGridComponent } from './components/playlists-grid/playlists-grid.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';

export const routes: Routes = [
  { path: '', component: PlaylistsGridComponent },
  { path: 'playlist/:id', component: PlaylistDetailsComponent }
];

