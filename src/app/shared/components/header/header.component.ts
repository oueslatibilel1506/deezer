import { Component } from '@angular/core';
import { SharedModule } from '../../modules/shared/shared.module';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'deezer-header',
  standalone: true,
  imports: [SharedModule,RouterModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {

}
