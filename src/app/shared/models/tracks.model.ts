export interface ITracks {
  id: number;
  title: string;
  duration: number;
  artist: {
    name: string;
  }
}
