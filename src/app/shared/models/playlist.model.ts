export interface IPlaylist {
  id: number;
  title: string;
  duration: number;
  creator: {
    name: string;
  }
  picture_xl: string;
}
