export interface IUsers {
  id: number,
  title: string;
  duration: number;
  picture_medium: string;
  creator: {
    id: number,
    name: string,
  },
}
