import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'secondsToTime'
})
export class SecondsToTimePipe implements PipeTransform {

  transform(value: number): string {
    if (value) {
      const hours: number = Math.floor(value / 3600);
      const minutes: number = Math.floor((value % 3600) / 60);
      const seconds: number = Math.floor(value % 60);

      const HH: string = this.pad(hours);
      const MM: string = this.pad(minutes);
      const SS: string = this.pad(seconds);

      return `${HH}:${MM}:${SS}`;
    }
    return '---'
  }

  private pad(num: number): string {
    return num < 10 ? '0' + num : num.toString();
  }
}
