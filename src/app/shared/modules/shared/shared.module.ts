import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { SecondsToTimePipe } from '../../pipes/second-to-time.pipe';


@NgModule({
  declarations: [SecondsToTimePipe],
  imports: [
    CommonModule,
    /* material */
    RouterModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatProgressSpinnerModule
  ],
  exports:[
    CommonModule,
    RouterModule,
    /* material */
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    /* pipe */
    SecondsToTimePipe
  ]
})
export class SharedModule { }
