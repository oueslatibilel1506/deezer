import Swal from 'sweetalert2';
import { TestBed } from '@angular/core/testing';
import { DeezerService } from './deezer.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('DeezerService', () => {
  let service: DeezerService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DeezerService]
    });
    service = TestBed.inject(DeezerService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch user playlists', () => {
    const userId = 1;
    const mockResponse = { data: [{ id: 1, title: 'Playlist 1', duration: 1000, picture_medium: 'url', creator: { id: 1, name: 'Creator 1' } }] };

    service.getUserPlaylists(userId).subscribe(playlists => {
      expect(playlists).toEqual(mockResponse);
    });

    const req = httpTestingController.expectOne(`https://api.deezer.com/user/${userId}/playlists`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockResponse);
  });

  it('should fetch playlist details', () => {
    const playlistId = 1;
    const mockResponse = { id: 1, title: 'Playlist 1', duration: 1000, creator: { name: 'Creator 1' }, picture_xl: 'url' };

    service.getPlaylistDetails(playlistId).subscribe(details => {
      expect(details).toEqual(mockResponse);
    });

    const req = httpTestingController.expectOne(`https://api.deezer.com/playlist/${playlistId}`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockResponse);
  });

  it('should fetch playlist tracks', () => {
    const playlistId = 1;
    const mockResponse = { data: [{ id: 1, title: 'Track 1', duration: 1000, artist: { name: 'Artist 1' } }] };

    service.getPlaylistTracks(playlistId).subscribe(tracks => {
      expect(tracks).toEqual(mockResponse);
    });

    const req = httpTestingController.expectOne(`https://api.deezer.com/playlist/${playlistId}/tracks`);
    expect(req.request.method).toEqual('GET');
    req.flush(mockResponse);
  });

  it('should handle HTTP errors', () => {
    const errorResponse = new ErrorEvent('Test error', {
      message: 'Test error message',
    });

    spyOn(Swal, 'fire');

    service.getPlaylistDetails(1).subscribe({
      error: (err) => {
        expect(err).toBeTruthy();
        expect(Swal.fire).toHaveBeenCalledOnceWith('error', 'Oops...', err.message);
      }
    });

    const req = httpTestingController.expectOne(`https://api.deezer.com/playlist/1`);
    expect(req.request.method).toEqual('GET');
    req.error(errorResponse);
  });
});
