import Swal from 'sweetalert2';
import { Observable, catchError } from 'rxjs';
import { Injectable, signal } from '@angular/core';
import { IUsers } from '../shared/models/users.model';
import { ITracks } from '../shared/models/tracks.model';
import { IPlaylist } from '../shared/models/playlist.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DeezerService {

  private apiUrl = 'https://api.deezer.com';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true, // Add this line for CORS support
    observe: 'body' as const,
    responseType: 'json' as const
  };

  constructor(private http: HttpClient) { }
  public isLoading = signal(true);
  getUserPlaylists(userId: number): Observable<{ data: IUsers[] }> {
    return this.httpGet<{ data: IUsers[] }>(`${this.apiUrl}/user/${userId}/playlists`);
  }

  getPlaylistDetails(playlistId: number): Observable<IPlaylist> {
    return this.httpGet<IPlaylist>(`${this.apiUrl}/playlist/${playlistId}`);
  }

  getPlaylistTracks(playlistId: number): Observable<{ data: ITracks[] }> {
    return this.httpGet<{ data: ITracks[] }>(`${this.apiUrl}/playlist/${playlistId}/tracks`);
  }
  /* error */

  public lunchSwal(type: 'error' | 'success', title: string, text: string) {
    Swal.fire({
      icon: type,
      title: title,
      text: text,
    });
  }

  private httpGet<T>(url: string): Observable<T> {
    return this.http.get<T>(url, this.httpOptions).pipe(catchError(err => {
      this.isLoading.set(false);
      this.lunchSwal('error', 'Oops...', err?.message);
      throw { err };

    }));
  }



}
